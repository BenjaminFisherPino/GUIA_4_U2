#include <iostream>
#include <fstream>
using namespace std;

//C L A S E  N O D O-------------------------------------
//ESTRUCTURA NODO (pasar a clase)
struct Nodo{
	int numero;
	Nodo *izq, *der;
};

Nodo *crearNodo(int dato) {
    Nodo *q;
    q = new Nodo();
    q->izq = NULL;
    q->der = NULL;
    q->numero = dato;
    return q;
}

void insertar_numero(Nodo *&arbol, int num){
	if (arbol == NULL) {
		Nodo *new_nodo = crearNodo(num);
		arbol = new_nodo;
	} else{
		int valor_raiz = arbol->numero;
		if (num > valor_raiz){
			insertar_numero(arbol->der, num);
		}else if (num <valor_raiz){
			insertar_numero(arbol->izq, num);
		} else{}
	}
}

void eliminar_nodo(Nodo *&arbol, int num){
	if (arbol != NULL){
		if (num < arbol->numero){
			eliminar_nodo(arbol->izq, num);
		} else{
			if (num > arbol->numero){
				eliminar_nodo(arbol->der, num);
			} else{
				Nodo *temp;
				temp = arbol;
				if (temp->der == NULL){
					arbol = temp->izq;
				} else{
					if (temp->izq == NULL){
						arbol = temp->der;
					} else{
						Nodo *aux, *aux1;
						bool bo = false;
						aux = arbol->izq;
						while (aux->der != NULL){
							aux1 = aux;
							aux = aux->der;
							bo = true;
						}
						
						arbol->numero = aux->numero;
						temp = aux;
						if (bo){
							aux1->der = aux->izq;
						} else{
							arbol->izq = aux->izq;
						}
					}
				}
			}
		}
	}
}
//----------------------------------------------------------

//C L A S E  G R A F O----------------------------------------
class Grafo {
    private:
    
    public:
        Grafo (Nodo *nodo) {
            ofstream fp;
            
            /* abre archivo */
            fp.open ("grafo.txt");

            fp << "digraph G {" << endl;
            fp << "node [style=filled fillcolor=yellow];" << endl;
                
            recorrer(nodo, fp);

            fp << "}" << endl;

            /* cierra archivo */
            fp.close();
                    
            /* genera el grafo */
            system("dot -Tpng -ografo.png grafo.txt &");
            
            /* visualiza el grafo */
            system("eog grafo.png &");
        }
        
        /*
        * recorre en árbol en preorden y agrega datos al archivo.
        */
        void recorrer(Nodo *arbol, ofstream &fp) {
			if (arbol != NULL){
				if (arbol->izq != NULL){
					fp << "\n" << arbol->numero << "->" << arbol->izq->numero << ";";
				} else {
					fp << "\n" << '"' << arbol->numero << "i" << '"' << " [shape=point];";
					fp << "\n" << arbol->numero << "->" << '"' << arbol->numero << "i" << '"';
				}
				
				if (arbol->der != NULL){
					fp << "\n" << arbol->numero << "->" << arbol->der->numero << ";";
				} else{
					fp << "\n" << '"' << arbol->numero << "d" << '"' << " [shape=point];";
					fp << "\n" << arbol->numero << "->" << '"' << arbol->numero << "d" << '"';
				}
				recorrer(arbol->izq, fp);
				recorrer(arbol->der, fp);
			}
		}
};
//----------------------------------------------------------------------

//C L A S E  N O D O----------------------------------------------------
//Sirve para imprimir el arbol
void mostrar_arbol(Nodo *&arbol, int cont){
	if (arbol == NULL){
		return;
	} else{
		mostrar_arbol(arbol->der, cont+1);
		for (int i = 0; i < cont;i++){
			cout << "   ";
		}
		cout << arbol->numero << endl;
		mostrar_arbol(arbol->izq, cont+1);
	}
}

//ORDENAR
void preorden(Nodo *arbol){//PREORDEN
	if (arbol == NULL){
		return;
	} else{
		cout << arbol->numero << " ";
		preorden(arbol->izq);
		preorden(arbol->der);
	}
}

void inorden(Nodo *arbol){//INORDEN
	if (arbol == NULL){
		return;
	} else{
		preorden(arbol->izq);
		cout << arbol->numero << " ";
		preorden(arbol->der);
	}
}

void postorden(Nodo *arbol){//POSTORDEN
     if(arbol != NULL){
          postorden(arbol->izq);
          postorden(arbol->der);
          cout << arbol->numero << " ";
     }
}

void imprimir_arbol(Nodo *arbol){
	int num;
	cout << "→ [1] Preorden" << endl;
	cout << "→ [2] Inorden" << endl;
	cout << "→ [3] Posorden" << endl;
	cin >> num;
	
	if (num == 1){//PREORDEN
		preorden(arbol);
		cout << endl;
	}else if (num == 2){//INORDEN
		inorden(arbol);
		cout << endl;
	}else if (num == 3){//POSORDEN
		postorden(arbol);
		cout << endl;
	}else{}//OPCION INVALIDA
}

//cambiar nombre a modificar!
void buscar(Nodo *&arbol, int num){
	if (num < arbol->numero){
		if (arbol->izq == NULL){
			cout << num << " no se encuentra en la lista" << endl;
		} else{
			buscar(arbol->izq,num);
		}
	} else{
		if (num > arbol->numero){
			if (arbol->der == NULL){
				cout << num << " no se enceuentra en la lista" << endl;
			} else{
				buscar(arbol->der, num);
			}
		}
		else{
			cout << num << " se encuentra en la lista" << endl;
			eliminar_nodo(arbol, num);
			cin >> num;
			insertar_numero(arbol,num);
		}
	}
}

//----------------------------------------------------------------------

//FUNCION MAIN
int main (void) {
	
	Nodo *arbol = NULL;
	int opc, num = 1;
	int cont = 0;
	cout << "Bienvenido!" << endl;
	
	//CICLO PRINCIPAL
	while (opc != 0){
		cout << "→ [1] Ingresar numero" << endl;
		cout << "→ [2] Eliminar numero" << endl;
		cout << "→ [3] Modificar numero" << endl;
		cout << "→ [4] Mostrar contenido del arbol" << endl;
		cout << "→ [5] Mostrar grafo" << endl;
		cout << "→ [6] Salir del programa" << endl;
		
		cin >> opc;
		
		if (opc == 1){
			cout << "Ingrese el numero: ";
			cin >> num;
			insertar_numero(arbol, num);
			
		}else if (opc == 2){
			int num;
			cin >> num;
			eliminar_nodo(arbol,num);
			
		}else if (opc == 3){
			int num = 0;
			cin >> num;
			buscar(arbol,num);
			
		}else if (opc == 4){
			//Imprimir informacion de l arbol
			imprimir_arbol(arbol);
			
		}else if (opc == 5){
			//Hacer y mostrar el grafo
			Grafo *g = new Grafo(arbol);
			
		}else if (opc == 6){
			//Salir del programa
			opc = 0;
			
		}else{
			//Opcion invalida
			cout << "Opcion invalida..." << endl;
		}
	}
    return 0;
}

