# Archivo README.md

**Versión 1.0**

Archivo README.md para guía numero 4 de Algoritmos y Estructuras de Datos

Profesor: Alejandro Mauricio Valdes Jimenez

Asignatura: Algoritmos y Estructuras de Datos

Autor: Benjamín Ignacio Fisher Pino

Fecha: 10/10/2021

---
## Resúmen del programa

Este programa fue hecho para el almacenamiento y visualizacion del contenido de un arbol binario (almacenamiento dinamico).

---
## Requerimientos

Para utilizar este programa se requiere un computador con sistema operativo Linux Ubuntu version 20.04 L, un compilador g++ version 9.3.0 y la version 4.2.1 de make. En caso de no tener el compilador de g++ o make y no saber como descargarlos, abajo se encuentra una pequeña guia de como descargarlos.

### Instalacion de g++

Para instalar g++ debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install g++"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para compilar codigos de c y c++.

### Instalacion de make

Para instalar make debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install make"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar un ejecutable del programa.

---
## Como instalarlo

Para descargar los archivos debe dirigirse al repositorio alocado en la siguiente URL.

Link: https://gitlab.com/BenjaminFisherPino/GUIA_4_U2.git

Tras ingresar al repositorio debe proceder a clonarlo ó a descargar los archivos manualmente, ambas opciones funcionan.Luego debe dirigirse a travez de su terminal a la ruta donde se encuentran los archivos descargados. Una vez ahi, usted debe ingresar a la carpeta  y escribir el siguiente comando en su terminal.

"make" y posteriormente "./proyecto"

En caso de tener problemas asegurece de tener la version 9.3.0 de g++ y la version 4.2.1 de make

Puede revisar con "g++ --version" y "make --version" respectivamente.

Si siguio todos los pasos debiese estar listo para ejecutar el programa!

---
## Funcionamiento del programa

El programa nos permite almacenar y revisar el contenido de un arbol binario. Dentro de las opciones de este programa se encuentra, agregar numero, eliminar numero, modificar informacion, imprimir el contenido en inorden, preorden o postorden, imprimir un grafo con la informacion del arbol binario y salir del programa. El codigo esta estructurado en base a una clase Main, la cual nos permite mantener el orden de ejecucion y compilacion. Se intento utilizar clases en archivos separados pero no resulto su aplicaion, a pesar de eso, se logro construir todo el programa con clases separadas, pero dentro de la clase Main. 

---

## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca
